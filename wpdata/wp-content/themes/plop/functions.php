<?php

/* Child theme declaration */

function plop_wp_enqueue_scripts() {

    $parenthandle = 'twentytwenty-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', // https://plop.org/wp-content/themes/twentytwenty/style.css
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'plop-style',
        get_stylesheet_uri(), // https://plop.org/wp-content/themes/plop/style.css
        array( $parenthandle ),
        $theme->get( 'Version' )
    );
}

add_action( 'wp_enqueue_scripts', 'plop_wp_enqueue_scripts' );


function plop_register_post_type_project(){
    register_post_type(
        "project",
        array(
            "labels" => array(
                'name' => 'Projet',
                'singular_name' => 'Projets'
            ),
            'description' =>         true,
            'public' =>              true,
            'publicity_queryable' => true,
            'exclude_from_each' =>   true,
            'show_ui' =>             true,
            'show_in_rest' =>        true,
            'hierarchical' =>        true,
            'supports' => array('title', 'editor', 'thumbail','page-attributes'),
            'has_archive' => 'projets',
            'rewrite' => array('slug' => 'projet'),

        )
    );
}

add_action('init', 'plop_register_post_type_project', 10) ;

function plop_add_meta_boxes_project() {

    add_meta_box(
        'plop_mbox_project',            // Unique ID ; nom de la meta box
        'Infos complémentaires',        // Box title ; titre
        'plop_mbox_project_content',    // Content callback, must be of type callable ; nom d'une fonction d'affichage dans la meta box
        'project'                       // Post type ; type de contenu rattaché à la meta box
    );

}

add_action( 'add_meta_boxes', 'plop_add_meta_boxes_project' );

function plop_mbox_project_content( $post ) {
    // get plop meta value
    $plop_year = get_post_meta(
        $post->ID,
        'plop-year',
        true
    ) ;
    $plop_email = get_post_meta(
        $post->ID,
        'plop-email',
        true
    ) ;
    echo'
    <p>
    <label for="plop-year">Date de création
    <input type="number" value="' . $plop_year . '" id="plop-year" name="plop-year">
    </label>
    </p>
    ' ;
    echo'
    <p>
    <label for="plop-email">Adresse e-mail
    <input type="email" value="' . $plop_email . '" id="plop-email" name="plop-email">
    </label>
    </p>
    ' ;
}

// save post meta
function plop_save_post( $post_ID ) {
    if ( isset($_POST['plop-year']) && !empty($_POST['plop-year']) ) {
		update_post_meta(
			$post_ID,
			'plop-year',
			sanitize_text_field($_POST['plop-year'])
		);
	}
    if ( isset($_POST['plop-email']) && !empty($_POST['plop-email']) ) {
		update_post_meta(
			$post_ID,
			'plop-email',
			sanitize_email($_POST['plop-email'])
		);
	}
}

add_action('save_post', 'plop_save_post') ;
